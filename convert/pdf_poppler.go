package convert

import(
	"os"
	"io"
	"io/ioutil"
	"bufio"
	"errors"
	"strings"
	"fmt"
	"strconv"
	"sync"
	"path/filepath"
	//"os/exec"

	"golang.org/x/net/context"
)

// PopplerPdfClient is an PdfClient that use poppler-utils.
type PopplerPdfClient struct {
	// The ImageClient to use for images conversions.
	imageClient DocumentClient
}

// ConvertFromPath converts a pdf from the given path.
func (client *PopplerPdfClient) ConvertFromPath(ctx context.Context, path string) (*DocTxt, error) {
	if len(path) == 0 {
		return nil, errors.New("[path] argument is required to convert a pdf from path")
	}

	// Get number of pages

	infos, err := PopplerPdfInfo(path)
	if err != nil {
		return nil, err
	}
	nbPages, err := strconv.Atoi(infos["Pages"])
	if err != nil {
		return nil, err
	}

	// Determines if it's a scan

	fonts, err := PopplerPdfFonts(path)
	if err != nil {
		return nil, err
	}
	isScan := len(fonts) == 0

	// Prepare working directory

	workingDir := ""
	if isScan {
		workingDir, err = ioutil.TempDir("", "doctxtconvert-pdfpoppler-")
		if err != nil {
			return nil, err
		}
		defer os.RemoveAll(workingDir)
	}

	// Perform the conversion, page by page in parallel

	var pages = make([]*DocTxtPage, nbPages)
	var pagesErrors = make([]error, nbPages)

	var wg sync.WaitGroup
	wg.Add(nbPages)
	for i := 0; i < nbPages; i++ {
		go func(ctx context.Context, path string, page int) {
			defer wg.Done()

			var r *DocTxtPage
			var err error

			if !isScan {
				r, err = doConvertTextPage(ctx, path, page)
			} else {
				r, err = doConvertImagePage(ctx, client, path, workingDir, page)
			}

			if err == nil {
				pages[page-1] = r
			} else {
				pagesErrors[page-1] = err
			}
		}(ctx, path, i+1)
	}
	wg.Wait()

	var pagesErrorsText []string
	for _, e := range pagesErrors {
		if e != nil {
			pagesErrorsText = append(pagesErrorsText, e.Error())
		}
	}
	if len(pagesErrorsText) > 0 {
		return nil, errors.New(strings.Join(pagesErrorsText, "\n"))
	}
	
	return &DocTxt{Pages: pages}, nil
}

func doConvertTextPage(ctx context.Context, path string, page int) (*DocTxtPage, error) {
	text, err := PopplerPdfToText(path, page)
	if err != nil {
		return nil, err
	}

	return &DocTxtPage{Text: text}, nil
}

func doConvertImagePage(ctx context.Context, client *PopplerPdfClient, path string, workingDir string, page int) (*DocTxtPage, error) {
	if client.imageClient == nil {
		return nil, errors.New("An ImageClient is required for image convertions in pdf documents")
	}
	
	err := PopplerPdfImages(path, workingDir, page)
	if err != nil {
		return nil, err
	}

	imageName := fmt.Sprintf("-%03d-000.jpg", page)
	imagePath := filepath.Join(workingDir, imageName)

	doctxt, err := client.imageClient.ConvertFromPath(ctx, imagePath)
	if err != nil {
		return nil, err
	}
	if len(doctxt.Pages) != 1 {
		return nil, fmt.Errorf("Image convertion for page [%v] of pdf [%s] returns [%v] result(s)", page, path, len(doctxt.Pages))
	}

	return doctxt.Pages[0], nil
}

// ConvertFromReader converts a pdf from the given Reader.
func (client *PopplerPdfClient) ConvertFromReader(ctx context.Context, reader io.Reader) (*DocTxt, error) {
	
	// Create temp pdf file
	
	workingDir, err := ioutil.TempDir("", "doctxtconvert-pdfpoppler-")
	if err != nil {
		return nil, err
	}
	defer os.RemoveAll(workingDir)

	pdfPath := filepath.Join(workingDir, "document.pdf")

	pdfFile, err := os.Create(pdfPath)
    if err != nil {
        return nil, err
    }
    defer func() {
        if err := pdfFile.Close(); err != nil {
            panic(err)
        }
	}()
	
	writer := bufio.NewWriter(pdfFile)
	buf := make([]byte, 1024)

	_, err = io.CopyBuffer(writer, reader, buf)
	if err != nil {
        return nil, err
	}
	
	err = writer.Flush()
	if err != nil {
        return nil, err
	}
	
	// Convert

	return client.ConvertFromPath(ctx, pdfPath)
}

// Close closes the conversion client and its resources.
func (client *PopplerPdfClient) Close() error {
	if client.imageClient != nil {
		return client.imageClient.Close()
	}
	return nil
}

//
// Poppler
//

// PopplerPdfInfo runs poppler-utils pdfinfo.
func PopplerPdfInfo(path string) (map[string]string, error) {
	if len(path) == 0 {
		return nil, errors.New("[path] argument is required to get pdf infos")
	}

	output, err := command.Output("pdfinfo", path)
	if err != nil {
		return nil, err
	}

	infos := make(map[string]string)

	for _, info := range strings.Split(string(output), "\n") {
		if parts := strings.SplitN(info, ":", 2); len(parts) > 1 {
			infos[strings.TrimSpace(parts[0])] = strings.TrimSpace(parts[1])
		}
	}

	return infos, nil
}

// PopplerPdfFonts extract a list of fonts use in the pdf.
func PopplerPdfFonts(path string) ([]string, error) {
	if len(path) == 0 {
		return nil, errors.New("[path] argument is required to get pdf fonts")
	}

	cmd := "pdffonts %s | tail -n +3 | cut -d' ' -f1 | sort | uniq"
	output, err := command.Output("bash", "-c", fmt.Sprintf(cmd, path))
	if err != nil {
		return nil, err
	}

	out := string(output)
	if out == "" || out == "\n" {
		return make([]string, 0), nil
	}
	return strings.Split(out, "\n"), nil
}

// PopplerPdfToText extract text of pdf or one page of pdf.
func PopplerPdfToText(path string, page int) (string, error) {
	if len(path) == 0 {
		return "", errors.New("[path] argument is required to extract text from pdf")
	}

	var output []byte
	var err error
	
	if page > 0 {
		p := strconv.Itoa(page)
		output, err = command.Output("pdftotext", "-q", "-nopgbrk", "-enc", "UTF-8", "-eol", "unix", "-f", p, "-l", p, path, "-")
	} else {
		output, err = command.Output("pdftotext", "-q", "-nopgbrk", "-enc", "UTF-8", "-eol", "unix", path, "-")
	}
	if err != nil {
		return "", err
	}

	return string(output), nil
}

// PopplerPdfImages extract image of pdf or one page of pdf.
func PopplerPdfImages(path string, workingDir string, page int) error {
	if len(path) == 0 {
		return errors.New("[path] argument is required to extract image from pdf")
	}
	if len(workingDir) == 0 {
		return errors.New("[workingDir] argument is required to extract image from pdf")
	}

	if strings.LastIndex(workingDir, string(os.PathSeparator)) != len(workingDir)-1 {
		workingDir += string(os.PathSeparator)
	}

	var err error
	
	if page > 0 {
		p := strconv.Itoa(page)
		err = command.Run("pdfimages", "-j", "-p", "-f", p, "-l", p, path, workingDir)
	} else {
		err = command.Run("pdfimages", "-j", "-p", path, workingDir)
	}
	
	return err
}

//
// Builder
//

// PopplerPdfClientBuilder helps to build a PopplerPdfClient.
type PopplerPdfClientBuilder struct {
	parent      *PdfClientBuilder
}

// NewPopplerPdfClientBuilder creates a new PopplerPdfClientBuilder.
func NewPopplerPdfClientBuilder(parent *PdfClientBuilder) *PopplerPdfClientBuilder {
	return &PopplerPdfClientBuilder{parent: parent}
}

// And permits to continue the Client configuration.
func (builder *PopplerPdfClientBuilder) And() *ClientBuilder {
	return builder.parent.parent
}

// Build build the PopplerPdfClient.
func (builder *PopplerPdfClientBuilder) Build() *PopplerPdfClient {
	return &PopplerPdfClient{}
}
