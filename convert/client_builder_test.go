package convert

import "testing"

func TestClientBuilderWithImageClient(t *testing.T) {
	client := NewClientBuilder().ConvertImages().WithVision().And().Build()
	if client.ImageClient == nil {
		t.Fatal("The built client has no ImageClient, it's expected there have")
	}
}

func TestClientBuilderWithoutImageClient(t *testing.T) {
	client := NewClientBuilder().Build()
	if client.ImageClient != nil {
		t.Fatal("The built client has a ImageClient, it's expected there have not")
	}
}

func TestClientBuilderWithPdfClient(t *testing.T) {
	client := NewClientBuilder().ConvertPdf().WithPoppler().And().Build()
	if client.PdfClient == nil {
		t.Fatal("The built client has no PdfClient, it's expected there have")
	}
}

func TestClientBuilderWithoutPdfClient(t *testing.T) {
	client := NewClientBuilder().Build()
	if client.PdfClient != nil {
		t.Fatal("The built client has a PdfClient, it's expected there have not")
	}
}

func TestClientBuilderWithPdfClientAndDefaultImageClient(t *testing.T) {
	client := NewClientBuilder().ConvertImages().WithVision().And().ConvertPdf().WithPoppler().And().Build()
	if client.PdfClient == nil {
		t.Fatal("The built client has no PdfClient, it's expected there have")
	}
	
	switch pdfClient := client.PdfClient.(type) {
	case *PopplerPdfClient:
		if pdfClient.imageClient == nil {
			t.Fatal("There is no ImageClient in the built client")
		}
		switch imageClient := pdfClient.imageClient.(type) {
		case *VisionImageClient:
			// ok
			imageClient.Close()
		default:
			t.Fatal("The ImageClient is not the default")
		}
	default:
		t.Fatal("The built client is not [PopplerPdfClient]")
	}
}

func TestClientBuilderWithPdfClientAndImageClient(t *testing.T) {
	imageClient := &MockDocumentClient{
		CloseFunc: func() error {
			return nil
		},
	}

	client := NewClientBuilder().ConvertImages().WithVision().And().ConvertPdf().ImageClient(imageClient).WithPoppler().And().Build()
	if client.PdfClient == nil {
		t.Fatal("The built client has no PdfClient, it's expected there have")
	}
	
	switch pdfClient := client.PdfClient.(type) {
	case *PopplerPdfClient:
		if pdfClient.imageClient == nil {
			t.Fatal("There is no ImageClient in the built client")
		}
		switch imageClient := pdfClient.imageClient.(type) {
		case *MockDocumentClient:
			// ok
			imageClient.Close()
		default:
			t.Fatal("The ImageClient is not the default")
		}
	default:
		t.Fatal("The built client is not [PopplerPdfClient]")
	}
}
