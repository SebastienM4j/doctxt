package convert

import (
	"testing"
	"io"

	gax "github.com/googleapis/gax-go"
	"golang.org/x/net/context"
	"google.golang.org/api/option"
	pb "google.golang.org/genproto/googleapis/cloud/vision/v1"
	"google.golang.org/genproto/googleapis/rpc/status"
)

type MockVision struct {
	NewImageFromReaderFunc func(io.Reader) (*pb.Image, error)
	NewImageAnnotatorClientFunc func(context.Context, ...option.ClientOption) (ImageAnnotatorClient, error)
}

func (g *MockVision) NewImageFromReader(r io.Reader) (*pb.Image, error) {
	return g.NewImageFromReaderFunc(r)
}

func (g *MockVision) NewImageAnnotatorClient(ctx context.Context, opts ...option.ClientOption) (ImageAnnotatorClient, error) {
	return g.NewImageAnnotatorClientFunc(ctx, opts...)
}

type MockImageAnnotatorClient struct {
	AnnotateImageFunc func(context.Context, *pb.AnnotateImageRequest, ...gax.CallOption) (*pb.AnnotateImageResponse, error)
	CloseFunc func() error
}

func (m *MockImageAnnotatorClient) AnnotateImage(ctx context.Context, req *pb.AnnotateImageRequest, opts ...gax.CallOption) (*pb.AnnotateImageResponse, error) {
	return m.AnnotateImageFunc(ctx, req, opts...)
}

func (m *MockImageAnnotatorClient) Close() error {
	return m.CloseFunc()
}

func TestConvertFromPathArgs(t *testing.T) {
	imageClient := &VisionImageClient{}

	_, err := imageClient.ConvertFromPath(context.Background(), "")

	if err == nil {
		t.Errorf("An error is expected without path argument")
	}
}

func TestConvertFromReader(t *testing.T) {
	mockResponse := &pb.AnnotateImageResponse{
		FullTextAnnotation: &pb.TextAnnotation{
			Pages: []*pb.Page{
				&pb.Page{Width: 210, Height: 297},
			},
		},
		TextAnnotations: []*pb.EntityAnnotation{
			&pb.EntityAnnotation{
				Description: "Text Detected",
			},
			&pb.EntityAnnotation{
				Description: "Text",
			},
			&pb.EntityAnnotation{
				Description: "Detected",
			},
		},
	}

	var verifyRequest *pb.AnnotateImageRequest
	mockImageAnnotator := &MockImageAnnotatorClient{
		AnnotateImageFunc: func(ctx context.Context, req *pb.AnnotateImageRequest, opts ...gax.CallOption) (*pb.AnnotateImageResponse, error) {
			verifyRequest = req
			return mockResponse, nil
		},
		CloseFunc: func() error {
			return nil
		},
	}

	visionBak := vision
	vision = &MockVision{
		NewImageFromReaderFunc: func(r io.Reader) (*pb.Image, error) {
			return &pb.Image{}, nil
		},
		NewImageAnnotatorClientFunc: func(ctx context.Context, opts ...option.ClientOption) (ImageAnnotatorClient, error) {
			return mockImageAnnotator, nil
		},
	}
	defer func() { vision = visionBak }()

	imageClient := &VisionImageClient{}

	res, err := imageClient.ConvertFromReader(context.Background(), nil)

	if err != nil {
		t.Fatalf("Unexpected error [%s]", err)
	}
	if res == nil {
		t.Fatal("Expected DocTxt but is nil")
	}

	if len(verifyRequest.Features) != 2 {
		t.Errorf("Expected [2] features for request, found [%b]", len(verifyRequest.Features))
	} else {
		featureText := false
		featureDocument := false
		for _, f := range verifyRequest.Features {
			switch f.Type {
			case pb.Feature_TEXT_DETECTION:
				featureText = true
			case pb.Feature_DOCUMENT_TEXT_DETECTION:
				featureDocument = true
			default:
				t.Errorf("Unexpected feature [%v] in the request", f.Type)
			}
		}
		if !featureText {
			t.Errorf("Expected feature [%v] not found in the request", pb.Feature_TEXT_DETECTION)
		}
		if !featureDocument {
			t.Errorf("Expected feature [%v] not found in the request", pb.Feature_DOCUMENT_TEXT_DETECTION)
		}
	}

	if len(res.Pages) != 1 {
		t.Fatalf("Expected [1] DocTxtPage, found [%b]", len(res.Pages))
	}

	resPage := res.Pages[0]

	expectedText := "Text Detected"
	if resPage.Text != expectedText {
		t.Errorf("Expected DocTxt.Text [%s], get [%s] instead", expectedText, resPage.Text)
	}

	if len(resPage.Annotations) != 2 {
		t.Fatalf("Expected [2] DocTxt.Annotations, found [%b]", len(resPage.Annotations))
	}
	_, found := resPage.Annotations["vision_page"]
	if !found {
		t.Errorf("Expected [vision_page] annotation but not found")
	}
	_, found = resPage.Annotations["vision_text"]
	if !found {
		t.Errorf("Expected [vision_text] annotation but not found")
	}
}

func TestConvertFromReaderVisionReturnError(t *testing.T) {
	mockResponse := &pb.AnnotateImageResponse{
		Error: &status.Status{
			Code: 400,
			Message: "Bad Request",
		},
	}

	verifyClosed := false
	mockImageAnnotator := &MockImageAnnotatorClient{
		AnnotateImageFunc: func(ctx context.Context, req *pb.AnnotateImageRequest, opts ...gax.CallOption) (*pb.AnnotateImageResponse, error) {
			return mockResponse, nil
		},
		CloseFunc: func() error {
			verifyClosed = true
			return nil
		},
	}

	var verifyOpts []option.ClientOption
	visionBak := vision
	vision = &MockVision{
		NewImageFromReaderFunc: func(r io.Reader) (*pb.Image, error) {
			return &pb.Image{}, nil
		},
		NewImageAnnotatorClientFunc: func(ctx context.Context, opts ...option.ClientOption) (ImageAnnotatorClient, error) {
			verifyOpts = opts
			return mockImageAnnotator, nil
		},
	}
	defer func() { vision = visionBak }()

	imageClient := &VisionImageClient{
		serviceAccountFile: "/tmp/service.json",
	}

	res, err := imageClient.ConvertFromReader(context.Background(), nil)

	if len(verifyOpts) != 1 {
		t.Errorf("Expected [1] option, found [%b]", len(verifyOpts))
	}

	if err == nil {
		t.Error("Expected an error")
	}
	if res != nil {
		t.Error("Expected DocTxt is nil")
	}
}

func TestConvertFromReaderCloseApiClient(t *testing.T) {
	mockResponse := &pb.AnnotateImageResponse{}

	verifyClosed := false
	mockImageAnnotator := &MockImageAnnotatorClient{
		AnnotateImageFunc: func(ctx context.Context, req *pb.AnnotateImageRequest, opts ...gax.CallOption) (*pb.AnnotateImageResponse, error) {
			return mockResponse, nil
		},
		CloseFunc: func() error {
			verifyClosed = true
			return nil
		},
	}

	visionBak := vision
	vision = &MockVision{
		NewImageFromReaderFunc: func(r io.Reader) (*pb.Image, error) {
			return &pb.Image{}, nil
		},
		NewImageAnnotatorClientFunc: func(ctx context.Context, opts ...option.ClientOption) (ImageAnnotatorClient, error) {
			return mockImageAnnotator, nil
		},
	}
	defer func() { vision = visionBak }()

	imageClient := &VisionImageClient{}

	imageClient.ConvertFromReader(context.Background(), nil)
	imageClient.Close()

	if !verifyClosed {
		t.Fail()
	}
}

func TestVisionImageClientBuilderWithoutServiceAccountFile(t *testing.T) {
	builder := NewVisionImageClientBuilder(nil)
	
	client := buildVisionImageClientBuilder(t, builder)
	
	if client.serviceAccountFile != "" {
		t.Errorf("Expects [] for client.serviceAccountFile but get [%s]", client.serviceAccountFile)
	}
}

func TestVisionImageClientBuilderWithServiceAccountFile(t *testing.T) {
	serviceAccountFile := "/fake/path/to/serviceaccount.json"

	builder := NewVisionImageClientBuilder(nil)
	builder.AuthWithServiceAccount(serviceAccountFile)
	
	client := buildVisionImageClientBuilder(t, builder)
	
	if client.serviceAccountFile != serviceAccountFile {
		t.Errorf("Expects [%s] for client.serviceAccountFile but get [%s]", serviceAccountFile, client.serviceAccountFile)
	}
}

func buildVisionImageClientBuilder(t *testing.T, builder *VisionImageClientBuilder) *VisionImageClient {
	client := builder.Build()
	if client == nil {
		t.Fatal("VisionImageClient is not build")
	}
	if client.apiClient != nil {
		t.Errorf("Expects that client.apiClient to be nil but not [%v]", client.apiClient)
	}
	return client
}
