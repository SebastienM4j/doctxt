package convert

import (
	"strings"
	"testing"
	"errors"
	"sync"
	"fmt"
	"io/ioutil"
	"path/filepath"

	"golang.org/x/net/context"
)

type MockCommand struct {
	OutputFunc func(name string, arg ...string) ([]byte, error)
	RunFunc func(name string, arg ...string) error
}

func (g *MockCommand) Output(name string, arg ...string) ([]byte, error) {
	return g.OutputFunc(name, arg...)
}

func (g *MockCommand) Run(name string, arg ...string) error {
	return g.RunFunc(name, arg...)
}

// ConvertFromPath

func TestPopplerPdfClientConvertFromPathArgs(t *testing.T) {
	popplerClient := &PopplerPdfClient{}

	_, err := popplerClient.ConvertFromPath(context.Background(), "")

	if err == nil {
		t.Errorf("An error is expected without path argument")
	}
}

func TestPopplerPdfClientConvertFromPathTextPdf(t *testing.T) {
	popplerClient := &PopplerPdfClient{}

	commandBak := command
	command = &MockCommand{
		OutputFunc: func(name string, arg ...string) ([]byte, error) {
			if name == "pdfinfo" {
				output := "Producer:       PDFlib+PDI 9.0.1 (JDK 1.4/SunOS)\n"
				output += "CreationDate:   Sun Sep  3 12:15:52 2017 CEST\n"
				output += "Form:           none\n"
				output += "JavaScript:     no\n"
				output += "Pages:          2\n"
				output += "Page size:      595 x 842 pts (A4)\n"
				output += "Page rot:       0\n"
				output += "File size:      30718 bytes\n"
				output += "PDF version:    1.4"
				return []byte(output), nil
			}
			if name == "bash" {
				output := "Courier\n"
				output += "Helvetica\n"
				output += "Helvetica-Bold\n"
				output += "Helvetica-BoldOblique\n"
				output += "Helvetica-Oblique\n"
				output += "Times-Roman\n"
				output += "ZapfDingbats"
				return []byte(output), nil
			}
			if name == "pdftotext" {
				for i, a := range arg {
					if a == "-f" {
						output := "Text of page "+arg[i+1]
						return []byte(output), nil
					}
				}
			}
			return nil, errors.New("Command unexpected")
		},
	}
	defer func() { command = commandBak }()

	doctxt, err := popplerClient.ConvertFromPath(context.Background(), "/path/to.pdf")

	if err != nil {
		t.Fatalf("Unexpected error [%s]", err)
	}

	if doctxt == nil {
		t.Fatal("Unexpected nil result")
	}

	if len(doctxt.Pages) != 2 {
		t.Fatalf("Expected [2] pages but get [%v]", len(doctxt.Pages))
	}

	expectedText := "Text of page 1"
	if doctxt.Pages[0].Text != expectedText {
		t.Errorf("Expected text [%s] for first page but get [%s]", expectedText, doctxt.Pages[0].Text)
	}

	expectedText = "Text of page 2"
	if doctxt.Pages[1].Text != expectedText {
		t.Errorf("Expected text [%s] for second page but get [%s]", expectedText, doctxt.Pages[1].Text)
	}
}

func TestPopplerPdfClientConvertFromPathTextPdfWithError(t *testing.T) {
	popplerClient := &PopplerPdfClient{}

	commandBak := command
	command = &MockCommand{
		OutputFunc: func(name string, arg ...string) ([]byte, error) {
			if name == "pdfinfo" {
				output := "Producer:       PDFlib+PDI 9.0.1 (JDK 1.4/SunOS)\n"
				output += "CreationDate:   Sun Sep  3 12:15:52 2017 CEST\n"
				output += "Form:           none\n"
				output += "JavaScript:     no\n"
				output += "Pages:          3\n"
				output += "Page size:      595 x 842 pts (A4)\n"
				output += "Page rot:       0\n"
				output += "File size:      30718 bytes\n"
				output += "PDF version:    1.4"
				return []byte(output), nil
			}
			if name == "bash" {
				output := "Courier\n"
				output += "Helvetica\n"
				output += "Helvetica-Bold\n"
				output += "Helvetica-BoldOblique\n"
				output += "Helvetica-Oblique\n"
				output += "Times-Roman\n"
				output += "ZapfDingbats"
				return []byte(output), nil
			}
			if name == "pdftotext" {
				for i, a := range arg {
					if a == "-f" {
						if arg[i+1] == "1" {
							output := "Text of page "+arg[i+1]
							return []byte(output), nil
						}
						return nil, errors.New("Error for page "+arg[i+1])
					}
				}
			}
			return nil, errors.New("Command unexpected")
		},
	}
	defer func() { command = commandBak }()

	doctxt, err := popplerClient.ConvertFromPath(context.Background(), "/path/to.pdf")

	expectedError := "Error for page 2\nError for page 3"
	if err == nil {
		t.Error("Unexpected nil error")
	} else if err.Error() != expectedError {
		t.Errorf("Expected error text [%s] but get [%s]", expectedError, err.Error())
	}

	if doctxt != nil {
		t.Fatal("Unexpected result")
	}
}

func TestPopplerPdfClientConvertFromPathScanPdf(t *testing.T) {
	popplerClient := &PopplerPdfClient{}

	var commandLock sync.Mutex
	verifyPdfImagesCalls := 0
	captureWorkingDir := ""

	commandBak := command
	command = &MockCommand{
		OutputFunc: func(name string, arg ...string) ([]byte, error) {
			if name == "pdfinfo" {
				output := "Producer:       PDFlib+PDI 9.0.1 (JDK 1.4/SunOS)\n"
				output += "CreationDate:   Sun Sep  3 12:15:52 2017 CEST\n"
				output += "Form:           none\n"
				output += "JavaScript:     no\n"
				output += "Pages:          2\n"
				output += "Page size:      595 x 842 pts (A4)\n"
				output += "Page rot:       0\n"
				output += "File size:      30718 bytes\n"
				output += "PDF version:    1.4"
				return []byte(output), nil
			}
			if name == "bash" {
				output := ""
				return []byte(output), nil
			}
			return nil, errors.New("Command unexpected")
		},
		RunFunc: func(name string, arg ...string) error {
			if name == "pdfimages" {
				commandLock.Lock()
				defer commandLock.Unlock()

				verifyPdfImagesCalls++
				if captureWorkingDir == "" {
					captureWorkingDir = arg[len(arg)-1]
				}
				return nil
			}
			return errors.New("Command unexpected")
		},
	}
	defer func() { command = commandBak }()

	resultDoctxt := &DocTxt{
		Pages: []*DocTxtPage{
			&DocTxtPage{
				Text: "Text",
			},
		},
	}

	var imageClientLock sync.Mutex
	verifyImageClientCalls := 0
	captureImagePath := ""

	imageClient := &MockDocumentClient{
		ConvertFromPathFunc: func(ctx context.Context, path string) (*DocTxt, error) {
			imageClientLock.Lock()
			defer imageClientLock.Unlock()

			verifyImageClientCalls++
			if captureImagePath == "" {
				captureImagePath = path
			}
			return resultDoctxt, nil
		},
	}

	popplerClient.imageClient = imageClient


	doctxt, err := popplerClient.ConvertFromPath(context.Background(), "/path/to.pdf")


	if err != nil {
		t.Fatalf("Unexpected error [%s]", err)
	}
	if doctxt == nil {
		t.Fatal("Unexpected nil result")
	}

	if len(doctxt.Pages) != 2 {
		t.Fatalf("Expected [2] pages but get [%v]", len(doctxt.Pages))
	}

	expectedText := "Text"
	if doctxt.Pages[0].Text != expectedText {
		t.Errorf("Expected text [%s] for first page but get [%s]", expectedText, doctxt.Pages[0].Text)
	}
	if doctxt.Pages[1].Text != expectedText {
		t.Errorf("Expected text [%s] for second page but get [%s]", expectedText, doctxt.Pages[1].Text)
	}

	if verifyPdfImagesCalls != 2 {
		t.Errorf("Expected [2] calls of poppler pdfimages but get [%v]", verifyPdfImagesCalls)
	}
	if verifyImageClientCalls != 2 {
		t.Errorf("Expected [2] calls of poppler pdfimages but get [%v]", verifyImageClientCalls)
	}

	expectedImagePath1 := filepath.Join(captureWorkingDir, "-001-000.jpg")
	expectedImagePath2 := filepath.Join(captureWorkingDir, "-002-000.jpg")
	if captureImagePath != expectedImagePath1 &&
	   captureImagePath != expectedImagePath2 {
		t.Errorf("Expected on of [%s, %s] for image path but get [%s]", expectedImagePath1, expectedImagePath2, captureImagePath)
	}
}

// ConvertFromReader

func TestPopplerPdfClientConvertFromReader(t *testing.T) {
	popplerClient := &PopplerPdfClient{}

	expectedContent := "pdf"

	commandBak := command
	command = &MockCommand{
		OutputFunc: func(name string, arg ...string) ([]byte, error) {
			if name == "pdfinfo" {
				fileContent, err := ioutil.ReadFile(arg[0])
				if err != nil {
					return nil, err
				}
				if string(fileContent) != expectedContent {
					return nil, fmt.Errorf("Expect [%s] file content but get [%s]", expectedContent, string(fileContent))
				}

				output := "Producer:       PDFlib+PDI 9.0.1 (JDK 1.4/SunOS)\n"
				output += "CreationDate:   Sun Sep  3 12:15:52 2017 CEST\n"
				output += "Form:           none\n"
				output += "JavaScript:     no\n"
				output += "Pages:          2\n"
				output += "Page size:      595 x 842 pts (A4)\n"
				output += "Page rot:       0\n"
				output += "File size:      30718 bytes\n"
				output += "PDF version:    1.4"
				return []byte(output), nil
			}
			if name == "bash" {
				output := "Courier\n"
				output += "Helvetica\n"
				output += "Helvetica-Bold\n"
				output += "Helvetica-BoldOblique\n"
				output += "Helvetica-Oblique\n"
				output += "Times-Roman\n"
				output += "ZapfDingbats"
				return []byte(output), nil
			}
			if name == "pdftotext" {
				for i, a := range arg {
					if a == "-f" {
						output := "Text of page "+arg[i+1]
						return []byte(output), nil
					}
				}
			}
			return nil, errors.New("Command unexpected")
		},
	}
	defer func() { command = commandBak }()

	reader := strings.NewReader(expectedContent)
	doctxt, err := popplerClient.ConvertFromReader(context.Background(), reader)

	if err != nil {
		t.Fatalf("Unexpected error [%s]", err)
	}

	if doctxt == nil {
		t.Fatal("Unexpected nil result")
	}

	if len(doctxt.Pages) != 2 {
		t.Fatalf("Expected [2] pages but get [%v]", len(doctxt.Pages))
	}

	expectedText := "Text of page 1"
	if doctxt.Pages[0].Text != expectedText {
		t.Errorf("Expected text [%s] for first page but get [%s]", expectedText, doctxt.Pages[0].Text)
	}

	expectedText = "Text of page 2"
	if doctxt.Pages[1].Text != expectedText {
		t.Errorf("Expected text [%s] for second page but get [%s]", expectedText, doctxt.Pages[1].Text)
	}
}

// Close

func TestPopplerPdfClientCloseWithImageClient(t *testing.T) {
	verifyClosed := false
	imageClient := &MockDocumentClient{
		CloseFunc: func() error {
			verifyClosed = true
			return nil
		},
	}

	popplerClient := &PopplerPdfClient{imageClient: imageClient}

	err := popplerClient.Close()

	if err != nil {
		t.Fatal("Unexpected error")
	}

	if !verifyClosed {
		t.Fatal("The image client have not been closed")
	}
}

func TestPopplerPdfClientCloseWithoutImageClient(t *testing.T) {
	popplerClient := &PopplerPdfClient{}

	err := popplerClient.Close()

	if err != nil {
		t.Error("Unexpected error")
	}
}

//
// Poppler
//

func TestPopplerPdfInfoArgs(t *testing.T) {

	_, err := PopplerPdfInfo("")

	if err == nil {
		t.Errorf("An error is expected without path argument")
	}
}

func TestPopplerPdfInfo(t *testing.T) {
	commandBak := command
	command = &MockCommand{
		OutputFunc: func(name string, arg ...string) ([]byte, error) {
			output := "Producer:       PDFlib+PDI 9.0.1 (JDK 1.4/SunOS)\n"
			output += "CreationDate:   Sun Sep  3 12:15:52 2017 CEST\n"
			output += "Form:           none\n"
			output += "JavaScript:     no\n"
			output += "Pages:          2\n"
			output += "Page size:      595 x 842 pts (A4)\n"
			output += "Page rot:       0\n"
			output += "File size:      30718 bytes\n"
			output += "PDF version:    1.4"
			return []byte(output), nil
		},
	}
	defer func() { command = commandBak }()

	infos, _ := PopplerPdfInfo("/path/to.pdf")

	expected := make(map[string]string)
	expected["Producer"] = "PDFlib+PDI 9.0.1 (JDK 1.4/SunOS)"
	expected["CreationDate"] = "Sun Sep  3 12:15:52 2017 CEST"
	expected["Form"] = "none"
	expected["JavaScript"] = "no"
	expected["Pages"] = "2"
	expected["Page size"] = "595 x 842 pts (A4)"
	expected["Page rot"] = "0"
	expected["File size"] = "30718 bytes"
	expected["PDF version"] = "1.4"

	for expK, expV := range expected {
		value, ok := infos[expK]
		if !ok {
			t.Errorf("Excepted key [%s] not found", expK)
		}
		if value != expV {
			t.Errorf("Excepted value [%s] for key [%s], but get [%s]", expV, expK, value)
		}
	}
}

func TestPopplerPdfFontsArgs(t *testing.T) {

	_, err := PopplerPdfFonts("")

	if err == nil {
		t.Errorf("An error is expected without path argument")
	}
}

func TestPopplerPdfFonts(t *testing.T) {
	commandBak := command
	command = &MockCommand{
		OutputFunc: func(name string, arg ...string) ([]byte, error) {
			output := "Courier\n"
			output += "Helvetica\n"
			output += "Helvetica-Bold\n"
			output += "Helvetica-BoldOblique\n"
			output += "Helvetica-Oblique\n"
			output += "Times-Roman\n"
			output += "ZapfDingbats"
			return []byte(output), nil
		},
	}
	defer func() { command = commandBak }()

	fonts, _ := PopplerPdfFonts("/path/to.pdf")

	expected := make([]string, 7)
	expected[0] = "Courier"
	expected[1] = "Helvetica"
	expected[2] = "Helvetica-Bold"
	expected[3] = "Helvetica-BoldOblique"
	expected[4] = "Helvetica-Oblique"
	expected[5] = "Times-Roman"
	expected[6] = "ZapfDingbats"

	for i, expV := range expected {
		value := fonts[i]
		if value != expV {
			t.Errorf("Excepted value [%s] at line [%v], but get [%s]", expV, i, value)
		}
	}
}

func TestPopplerPdfFontsNoFonts(t *testing.T) {
	commandBak := command
	command = &MockCommand{
		OutputFunc: func(name string, arg ...string) ([]byte, error) {
			output := ""
			return []byte(output), nil
		},
	}
	defer func() { command = commandBak }()

	fonts, _ := PopplerPdfFonts("/path/to.pdf")

	if fonts == nil {
		t.Fatal("Unexpected nil result")
	}
	if len(fonts) != 0 {
		t.Fatalf("Expected [0] fonts, get [%v]", len(fonts))
	}
}

func TestPopplerPdfToTextArgs(t *testing.T) {
	_, err := PopplerPdfToText("", 0)

	if err == nil {
		t.Errorf("An error is expected without path argument")
	}
}

func TestPopplerPdfToTextAllPages(t *testing.T) {
	expectedText := "Extracted text !"

	var verifyCommandArgs []string
	commandBak := command
	command = &MockCommand{
		OutputFunc: func(name string, arg ...string) ([]byte, error) {
			verifyCommandArgs = arg
			output := expectedText
			return []byte(output), nil
		},
	}
	defer func() { command = commandBak }()

	text, _ := PopplerPdfToText("/path/to.pdf", 0)

	if text != expectedText {
		t.Errorf("Excepted text [%s], but get [%s]", expectedText, text)
	}

	for _, arg := range verifyCommandArgs {
		if arg == "-f" || arg == "-l" {
			t.Error("No expected args found")
		}
	}
}

func TestPopplerPdfToTextOnePage(t *testing.T) {
	expectedText := "Extracted text !"

	var verifyCommandArgs []string
	commandBak := command
	command = &MockCommand{
		OutputFunc: func(name string, arg ...string) ([]byte, error) {
			verifyCommandArgs = arg
			output := expectedText
			return []byte(output), nil
		},
	}
	defer func() { command = commandBak }()

	text, _ := PopplerPdfToText("/path/to.pdf", 2)

	if text != expectedText {
		t.Errorf("Excepted text [%s], but get [%s]", expectedText, text)
	}

	f := false
	l := false
	for i, arg := range verifyCommandArgs {
		if arg == "-f" {
			f = true
			if verifyCommandArgs[i+1] != "2" {
				t.Errorf("First page argument [%s] is not the expected [%s]", verifyCommandArgs[i+1], "2")
			}
		}
		if arg == "-l" {
			l = true
			if verifyCommandArgs[i+1] != "2" {
				t.Errorf("Last page argument [%s] is not the expected [%s]", verifyCommandArgs[i+1], "2")
			}
		}
	}
	if !f {
		t.Error("First page argument not found")
	}
	if !l {
		t.Error("Last page argument not found")
	}
}

func TestPopplerPdfImagesArgs(t *testing.T) {
	err := PopplerPdfImages("", "", 0)
	if err == nil {
		t.Errorf("An error is expected without path argument")
	}

	err = PopplerPdfImages("/path/to.pdf", "", 0)
	if err == nil {
		t.Errorf("An error is expected without workingDir argument")
	}
}

func TestPopplerPdfImagesAllPages(t *testing.T) {
	var verifyCommandArgs []string
	commandBak := command
	command = &MockCommand{
		RunFunc: func(name string, arg ...string) error {
			verifyCommandArgs = arg
			return nil
		},
	}
	defer func() { command = commandBak }()

	PopplerPdfImages("/path/to.pdf", "/tmp", 0)

	for _, arg := range verifyCommandArgs {
		if arg == "-f" || arg == "-l" {
			t.Error("No expected args found")
		}
	}
}

func TestPopplerPdfImagesOnePage(t *testing.T) {
	var verifyCommandArgs []string
	commandBak := command
	command = &MockCommand{
		RunFunc: func(name string, arg ...string) error {
			verifyCommandArgs = arg
			return nil
		},
	}
	defer func() { command = commandBak }()

	PopplerPdfImages("/path/to.pdf", "/tmp", 2)

	f := false
	l := false
	for i, arg := range verifyCommandArgs {
		if arg == "-f" {
			f = true
			if verifyCommandArgs[i+1] != "2" {
				t.Errorf("First page argument [%s] is not the expected [%s]", verifyCommandArgs[i+1], "2")
			}
		}
		if arg == "-l" {
			l = true
			if verifyCommandArgs[i+1] != "2" {
				t.Errorf("Last page argument [%s] is not the expected [%s]", verifyCommandArgs[i+1], "2")
			}
		}
	}
	if !f {
		t.Error("First page argument not found")
	}
	if !l {
		t.Error("Last page argument not found")
	}
}
