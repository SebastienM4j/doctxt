package convert

import (
	"io"

	govision "cloud.google.com/go/vision/apiv1"
	gax "github.com/googleapis/gax-go"
	"golang.org/x/net/context"
	"google.golang.org/api/option"
	pb "google.golang.org/genproto/googleapis/cloud/vision/v1"
)

var vision Vision = &GoVision{}

// Vision defines methods used on "cloud.google.com/go/vision/apiv1" package
type Vision interface {
	NewImageFromReader(io.Reader) (*pb.Image, error)
	NewImageAnnotatorClient(context.Context, ...option.ClientOption) (ImageAnnotatorClient, error)
}

// GoVision implements Vision interface with "cloud.google.com/go/vision/apiv1" package
type GoVision struct {}

// NewImageFromReader cf NewImageFromReader from real package
func (g *GoVision) NewImageFromReader(r io.Reader) (*pb.Image, error) {
	return govision.NewImageFromReader(r)
}

// NewImageAnnotatorClient cf NewImageAnnotatorClient from real package
func (g *GoVision) NewImageAnnotatorClient(ctx context.Context, opts ...option.ClientOption) (ImageAnnotatorClient, error) {
	return govision.NewImageAnnotatorClient(ctx, opts...)
}

// ImageAnnotatorClient defines an interface
// for the Google Vision API ImageAnnotatorClient
type ImageAnnotatorClient interface {
	AnnotateImage(context.Context, *pb.AnnotateImageRequest, ...gax.CallOption) (*pb.AnnotateImageResponse, error)
	Close() error
}
