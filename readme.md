DocTxt
======

[![Release](https://img.shields.io/badge/release-v1.0.0-blue.svg)](https://gitlab.com/SebastienM4j/doctxt/blob/master/changelog.md) [![GitLab pipeline status](https://gitlab.com/SebastienM4j/doctxt/badges/master/build.svg)](https://gitlab.com/SebastienM4j/doctxt/commits/master) [![GitLab coverage report](https://gitlab.com/SebastienM4j/doctxt/badges/master/coverage.svg)](https://gitlab.com/SebastienM4j/doctxt/commits/master) [![License](https://img.shields.io/dub/l/vibe-d.svg)](https://gitlab.com/SebastienM4j/doctxt/blob/master/LICENSE) [![GoDoc](https://godoc.org/github.com/SebastienM4j/doctxt/convert?status.svg)](https://godoc.org/github.com/SebastienM4j/doctxt/convert)

A tool to extract the text of a document that is in a complex format (PDF, image, ...).

Currently supported formats :

* PDF (text and scan)
* Images

It use [Google Vision API](https://cloud.google.com/vision/docs/) for optical character recognition (OCR) and [poppler-utils](https://launchpad.net/ubuntu/+source/poppler) to manipulate pdf files.

DocTxt is written in [Go](https://golang.org/).


Installation
------------

### Prerequisites

* [Install Go](https://golang.org/doc/install) (>= 1.9.2)
* [Configure `$GOPATH/bin`](https://golang.org/doc/code.html#GOPATH) in the `PATH` environment variable.

### Installation

```shell
go get gitlab.com/SebastienM4j/doctxt/...
```

This will fetch the source code and build binary into `$GOPATH/bin`.

Check installation with :

```shell
doctxt -h
```

### Dependencies

#### Google Vision API

* [Create a Google service account](https://cloud.google.com/iam/docs/creating-managing-service-accounts#creating_a_service_account)
* Download the JSON key file
* Optionally, export `GOOGLE_APPLICATION_CREDENTIALS` environment variable with the path of key file (see [documentation](https://cloud.google.com/vision/docs/auth))

#### Poppler Utils

Install `poppler-utils` :

```shell
# Debian / Ubuntu
sudo apt-get install poppler-utils
```


Usage
-----

### As binary

DocTxt provide a command-line interface (CLI).

To convert a document into text :

```shell
doctxt /home/user/document.pdf
doctxt -i /home/user/image.jpg
doctxt --input /home/user/document.pdf
```

The result is in JSON format :

```javascript
{
    "pages": [
        {
            "text": "text of the first page",
            "annotations": {
                "vision_page": {
                    // Google Vision API TextAnnotation type
                    // https://cloud.google.com/vision/docs/reference/rest/v1/images/annotate#TextAnnotation
                },
                "vision_text": {
                    // Google Vision API EntityAnnotation type
                    // https://cloud.google.com/vision/docs/reference/rest/v1/images/annotate#EntityAnnotation
                }
            }
        },
        {
            "text": "text of the seconde page"
        }
    ]
}
```

By default the result is print on the standard out.

If you want output in a file :

```shell
doctxt /home/user/document.pdf > /home/user/document.json
doctxt -i /home/user/image.jpg -o /home/user/image.json
doctxt --input /home/user/document.pdf --output /home/user/document.json
```

The service account file of Google Vision API is by default loaded at path given in `GOOGLE_APPLICATION_CREDENTIALS`  environment variable.

Otherwise, it could be given in the command line :

```shell
doctxt -vak /home/user/key.json /home/user/document.pdf
doctxt --vision.auth.key /home/user/key.json /home/user/document.pdf
```

### As library

The `doctxt/convert` package could be used as a library in other Go project.

Import packages :

```go
import (
    "gitlab.com/SebastienM4j/doctxt/convert"
    "golang.org/x/net/context"
)
```

Conversions are made with a `Client` witch have a `DocumentClient` for each type of supported documents :

```go
// Client is the client to use for conversions of any type of file.
// It should be closed when not longer use.
type Client struct {
	// The ImageClient to use for images conversions.
	ImageClient DocumentClient
	// The PdfClient to use for pdf conversions.
	PdfClient DocumentClient
}

// DocumentClient defines a client to use for document conversions.
type DocumentClient interface {
	// ConvertFromPath converts a document from the given path.
	ConvertFromPath(ctx context.Context, path string) (*DocTxt, error)
	// ConvertFromReader converts a document from the given Reader.
	ConvertFromReader(ctx context.Context, reader io.Reader) (*DocTxt, error)
	// Close closes the conversion client and its resources
	Close() error
}
```

Create and configure the `Client` with the `ClientBuilder` :

```go
clientBuilder := convert.NewClientBuilder()
clientBuilder.ConvertImages().WithVision().AuthWithServiceAccount(visionServiceAccountFile)
clientBuilder.ConvertPdf().WithPoppler()

client := clientBuilder.Build()
defer client.Close()
```

And then convert file from path or reader :

```go
doctxt, err := client.ConvertPath(context.Background(), inputPath)
```


Credits
-------

Inspired by https://github.com/sajari/docconv.


License
-------

DocTxt is released under [MIT license](https://gitlab.com/SebastienM4j/doctxt/blob/finalize/LICENSE).
